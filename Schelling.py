#!/usr/bin/env python


import matplotlib.pyplot as plt
import scipy as sp
import numpy as np
from random import random, choice

#This code simulates the Schelling Model of Segregation

class Schelling: 
    
	def __init__(self,S,counts): #Defining my initial variables
        
        	self.S = S #Size of the lattice
		self.counts=counts #Sweeps of lattice 
		self.array=self.myinitiallattice() #Allocating a name to my initial lattice

		
#Defining my "neighbourhood". 0's represent empty houses, 1's and -1's define two different ethnicities.	
	
	def myinitiallattice(self):  #A function to define my inital lattice 
        	matrix = np.zeros((self.S,self.S),dtype=int) #Initial array of zeros of size SxS
        	for y in range(self.S):
            		for x in range(self.S):
                		matrix[x,y] = np.random.choice([1,-1,0],p=[0.8,0.1,0.1]) #Each position in the matrix will be randomly allocated a value of 1,-1 or 0 based on varying probabilities. A higher probability of 1's, for example, means a higher population of that ethnicity in the neighbourhood.
        	
		return matrix
		
			
		
	def nearneigh(self,x,y): #This function sums up the "neighbours" around a house
		return self.array[(x + 1) % self.S, y] + self.array[(x - 1 + self.S) % self.S, y] + self.array[x, (y + 1) % self.S] + self.array[x, (y - 1 + self.S) % self.S]

	def Tolerance(self): #This function calculates the tolerance of the inhabitants.
		for i in range(self.counts):
			for x in range(self.S):
				for y in range(self.S):
					l=np.transpose(np.where(self.array == 0))
					if self.array[x,y].all() == -1: #If less than 25% of the same ethnicity, move out.
						if self.nearneigh(x,y) > -2:
							self.array[x,y] = 0
							l[0]=-1 #Move to a new house
				
					if self.array[x,y].all() == 1: #If less than 25% of the same ethnicity, move out
						if self.nearneigh(x,y) < 2:
							self.array[x,y] = 0
							l[0]=1 #Move to a new house
					
				
		print self.array
		

	def lattice(self): #This function plots the lattice
        	I=Schelling(self.S,self.counts)
		I.Tolerance()
        	self.array=I.array
		plt.imshow(self.array)
		plt.title("Schelling Model")

	def show(self): #The function shows the plots 
        	plt.show()


P=Schelling(64,5)
P.lattice()
P.show()



