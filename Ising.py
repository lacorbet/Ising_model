#!/usr/bin/env python


import matplotlib.pyplot as plt
import scipy as sp
import numpy as np
from random import random, choice

#This code simulates the Ising Model and calculates and plots the Energy, Magnetisation, Specific Heat Capacity and Magnetic Susceptability.

class Ising: 
    
	def __init__(self,S,T,counts,J): #Defining my initial variables
        
        	self.S = S #Size of the lattice
        	self.T = T #Temperature
		self.Kb = 1.0 #Boltzmann Constant
		self.counts=counts #Number of sweeps through the lattice 
		self.array=self.myinitiallattice() #Allocating a name to my initial lattice
		self.J=J


	def myinitiallattice(self):  #A function to define my inital lattice 
        	matrix = np.zeros((self.S,self.S),dtype=int) #Initial array of zeros of size SxS
        	for y in range(self.S):
            		for x in range(self.S):
                		matrix[x,y] = choice([1,-1]) #Each position x,y will be randomly allocated a value of 1 or -1
        	return matrix

    

    	def nearneigh(self,x,y): #This function sums up the spins of the nearest neighbours
		return self.array[(x + 1) % self.S, y] + self.array[(x - 1 + self.S) % self.S, y] + self.array[x, (y + 1) % self.S] + self.array[x, (y - 1 + self.S) % self.S]    

   
    	def Ham(self,x,y): #The hamiltonian that defines the energy of the system
        	return (-1.0*self.J * self.array[x,y]*(self.nearneigh(x,y)))

	def Ham_squared(self,x,y): #The hamiltonian squared for the specific heat calculation 
		return (-1.0 *self.J* self.array[x,y]*(self.nearneigh(x,y)))**2


    	def metal(self, counts): #Defining the Metropolis Algorithm
        	for i in range(counts):
        		for x in range(self.S):
      				for y in range(self.S):

            				dE = -2*self.J* self.Ham(x,y) #Definition for the change in energy

            				if (dE <= 0):
                				self.array [x,y] *= -1
                				
                
            				elif random() < np.exp(-1.0 * dE/(self.T)*(self.Kb)):
                				self.array[x,y] *= -1






#---------------------------------------------------------------------------------------------------------------------------------------------

                				
	#Defining Energy,Magnetisation, Magnetic Susceptability and Specific Heat Capacity

	def E_tot(self,myarray): #Defining my total energy over the whole lattice
		en=0
		for x in range(self.S):
			for y in range(self.S):
				en=en +0.5*self.Ham(x,y)
		return en/self.S**2

	def E_tot_squared(self,myarray): #Defining the energy squared for the specific heat capaciity
		en_sq=0
		for x in range(self.S):
			for y in range(self.S):
				en_sq=en_sq +0.25*self.Ham_squared(x,y)
		return en_sq/self.S**2

	def mag_tot(self,myarray): #Defining the total magnetisation over the whole lattice
		return (np.abs(np.sum(myarray)))/float(self.S**2)

	def mag_tot_squared(self,myarray): #Defining the total magnetisation squared for the magnetic susceptability
		return (np.sum(myarray**2))/float(self.S**2)

	def mag_suscept(self,myarray,T): #Defining the magnetic susceptability
		return((self.mag_tot(myarray**2))-(self.mag_tot(myarray)))/((self.Kb)*(T))
	

	def specific_heat(self,myarray,T): #Defining the specific heat capacity
		return((self.E_tot_squared(myarray))-(self.E_tot(myarray)**2))/((self.Kb)*(T**2))



#--------------------------------------------------------------------------------------------------------------------------------------------


	#Creating my plots 			
	 
    	def Energyplot(self): #This function plots the Average Energy Versus Temperature
		newenergy=[]	 #Creating an empty array to store the energy over certain time values
		for T in np.arange(0.1,4,0.1): 		#For loop over specified time intervals
			I=Ising(self.S,T,self.counts,self.J) 
			I.metal(self.counts)
			self.array=I.array
			newenergy.append(self.E_tot(self.array)) #Appending my empty array 
		
		plt.scatter(np.arange(0.1,4,0.1),newenergy) 	#Plotting the scatter graph
		plt.title("Graph of Energy versus Temperature at T=1")
		plt.ylabel("Energy(J)")
		plt.xlabel("Temperature (J/K$_B$)")

	def Magplot(self): #This function plots the average magnetisation versus temperature
		newmag=[]
		for T in np.arange(0.1,4.0,0.1):
			I=Ising(self.S,T,self.counts,self.J)
			I.metal(self.counts)
			self.array=I.array
			newmag.append(self.mag_tot(self.array))


		plt.scatter(np.arange(0.1,4,0.1),newmag)
		plt.title("Graph of Average Magnetisation versus Temperature at T=1")
		plt.ylabel("Average Magnetisation Per Site($\mu$)")
		plt.xlabel("Temperature (J/k$_B$)")


	def Mag_sus(self): #This function plots the magnetic susceptability versus temperature
		newmag_sus=[]
		for T in np.arange(0.1,6.0,0.1):
			I=Ising(self.S,T,self.counts,self.J)
			I.metal(self.counts)
			self.array=I.array
			newmag_sus.append(self.mag_suscept(self.array,T))
		
		plt.scatter(np.arange(0.1,6.0,0.1),newmag_sus)
		plt.title("Graph of Magnetic Susceptability versus Temperature at T=1")
		plt.ylabel("X($\mu$/K$_B$)")
		plt.xlabel("Temperature (J/k$_B$)")

	def Spec_Plot(self): #This function plots the specific heat capacity versus temperature
		spec_list=[]
		for T in np.arange(0.3,6.0,0.1):
			I=Ising(self.S,T,self.counts,self.J)
			I.metal(self.counts)
			self.array=I.array
			spec_list.append(self.specific_heat(self.array,T))
		
		plt.scatter(np.arange(0.3,6.0,0.1),spec_list)
		plt.title("Graph of Specific Heat Capacity versus Temperature at T=1")
		plt.ylabel("Energy(J)")
		plt.xlabel("Temperature (J/$k^2_B$)")

		
	def lattice(self): #This function plots the lattice
        	I=Ising(self.S,self.T,self.counts,self.J)
        	I.metal(self.counts)
        	self.array=I.array
		plt.imshow(self.array)
		plt.title("Ising Model at Equilibrium")


	def show(self): #The function shows the plots 
        	plt.show()

        


#P=Ising(64,1,3000,1)
#P.lattice()
#P.show()

#P=Ising(10,1,1000,1)
#P.Energyplot()
#P.show()


#P=Ising(10,1,1000,1)
#P.Magplot()
#P.show()

#P=Ising(10,1,1000,1)
#P.Mag_sus()
#P.show()

#P=Ising(10,1,1000,1)
#P.Spec_Plot()
#P.show()


