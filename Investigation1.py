#!/usr/bin/env python

import matplotlib.pyplot as plt
import scipy as sp
import numpy as np
from random import random, choice


#The purpose of this code is to identify the number of sweeps of the lattice needed in order to reach equibrium and subsequently to investigate phase transition with temperature 


#Below is the code from my initial Ising Model Simulation.

class Ising: 
    
	def __init__(self,S,T,counts,J): #Defining my initial variables
        
        	self.S = S #Size of the lattice
        	self.T = T #Temperature
		self.Kb = 1.0 #Boltzmann Constant
		self.counts=counts #Number of sweeps through the lattice 
		self.array=self.myinitiallattice() #Allocating a name to my initial lattice
		self.J=J


	def myinitiallattice(self):  #A function to define my inital lattice 
        	matrix = np.zeros((self.S,self.S),dtype=int) #Initial array of zeros of size SxS
        	for y in range(self.S):
            		for x in range(self.S):
                		matrix[x,y] = choice([1,-1]) #Each position x,y will be randomly allocated a value of 1 or -1
        	return matrix

    

    	def nearneigh(self,x,y): #This function sums up the spins of the nearest neighbours
		return self.array[(x + 1) % self.S, y] + self.array[(x - 1 + self.S) % self.S, y] + self.array[x, (y + 1) % self.S] + self.array[x, (y - 1 + self.S) % self.S]    

   
    	def Ham(self,x,y): #The hamiltonian that defines the energy of the system
        	return (-1.0 *self.J* self.array[x,y]*(self.nearneigh(x,y)))

	def Ham_squared(self,x,y): #The hamiltonian squared for the specific heat calculation 
		return (-1.0 *self.J* self.array[x,y]*(self.nearneigh(x,y)))**2


    	def metal(self, counts): #Defining the Metropolis Algorithm
        	for i in range(counts):
        		for x in range(self.S):
      				for y in range(self.S):

            				dE = -2*self.J* self.Ham(x,y) #Definition for the change in energy

            				if (dE <= 0):
                				self.array [x,y] *= -1
                				
                
            				elif random() < np.exp(-1.0 * dE/(self.T)*(self.Kb)):
                				self.array[x,y] *= -1



#---------------------------------------------------------------------------------------------------------------------------
#This part of the code calculates the average magnetisation over the lattice and then calculates the amount of sweeps for this value to reach equilibrium.

	def mag_tot(self,myarray): #Defining the average magnetisation over the whole lattice
		return (np.abs(np.sum(myarray)))/float(self.S**2)

	
	def No_sweeps(self): #This function plots average magnetisation against counts
		sweeps=[]
		for counts in np.arange(0,100,1):
			I=Ising(self.S,self.T,counts,self.J)
			I.metal(counts)
			self.array=I.array
			sweeps.append(self.mag_tot(self.array))

		plt.scatter(np.arange(0,100,1),sweeps)
		plt.ylabel("Average Magnetisation")
		plt.xlabel("Sweeps")
		plt.title("Average Magnetisation Versus Counts")




#-------------------------------------------------------------------------------------------------------------------------------		
#This part of the code shows the lattice at below, above and at the critical temperature.
		
	def lattice(self): #This function plots the lattice
        	I=Ising(self.S,T,self.counts,self.J)
        	I.metal(self.counts)
        	self.array=I.array
		plt.imshow(self.array)
		plt.title("Ising Model: T=%i" %T)
		

	def show(self): #The function shows the plots 
        	plt.show()

	
t_array=([1.0,2.3,4.0])
for T in t_array:
	P=Ising(100,T,1,1)
	P.lattice()
	P.show()     


P=Ising(64,1,10,1)
P.No_sweeps()
P.show()





